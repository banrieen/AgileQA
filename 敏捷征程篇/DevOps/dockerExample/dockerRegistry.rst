Registry Server Deployment
===========================================================

Author: Qinxing 2018-02026

Run a local registry
-----------------------------------------------------------

Use a command like the following to start the registry container::

  $ docker run -d -p 5000:5000 --restart=always --name registry registry:2

Update an image from Docker Hub to your registry
-----------------------------------------------------------

You can pull an image from Docker Hub and push it to your registry. The following example pulls the ubuntu:16.04 image from Docker Hub and re-tags it as my-ubuntu, then pushes it to the local registry. Finally, the ubuntu:16.04 and my-ubuntu images are deleted locally and the my-ubuntu image is pulled from the local registry.

Pull the ubuntu:16.04 image from Docker Hub::

  $ docker pull ubuntu:16.04

Tag the image as localhost:5000/my-ubuntu. This creates an additional tag for the existing image. When the first part of the tag is a hostname and port, Docker interprets this as the location of a registry, when pushing::

  $ docker tag ubuntu:16.04 localhost:5000/my-ubuntu

Push the image to the local registry running at localhost:5000::

  $ docker push localhost:5000/my-ubuntu

Remove the locally-cached ubuntu:16.04 and localhost:5000/my-ubuntu images, so that you can test pulling the image from your registry. This does not remove the localhost:5000/my-ubuntu image from your registry::

  $ docker image remove ubuntu:16.04
  $ docker image remove localhost:5000/my-ubuntu

Pull the localhost:5000/my-ubuntu image from your local registry::

  $ docker pull localhost:5000/my-ubuntu

Stop a local registry

To stop the registry, use the same docker container stop command as with any other container::

  $ docker container stop registry

To remove the container, use docker container rm::

  $ docker container stop registry && docker container rm -v registry


Run an externally-accessible registry
-----------------------------------------------------------

Deploy a plain HTTP registry
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  注意，如果指定镜像库忽略安全性，则会把 基于 CA 证书认证的 TLS 认证忽略，但是用户名和密码的认证还是存在的；相比较 个人认证证书要方便，当然比 公共CA认证缺少安全性。

  *Warning: It’s not possible to use an insecure registry with basic authentication.*

This procedure configures Docker to entirely disregard security for your registry. This is very insecure and is not recommended. It exposes your registry to trivial man-in-the-middle (MITM) attacks. Only use this solution for isolated testing or in a tightly controlled, air-gapped environment.

    Edit the daemon.json file, whose default location is /etc/docker/daemon.json on Linux or C:\ProgramData\docker\config\daemon.json on Windows Server. If you use Docker for Mac or Docker for Windows, click the Docker icon, choose Preferences, and choose +Daemon.

    If the daemon.json file does not exist, create it. Assuming there are no other settings in the file, it should have the following contents::

    {
      "insecure-registries" : ["myregistrydomain.com:5000"]
    }

    Substitute the address of your insecure registry for the one in the example.

    With insecure registries enabled, Docker goes through the following steps:
        + First, try using HTTPS.
            - If HTTPS is available but the certificate is invalid, ignore the error about the certificate.
            - If HTTPS is not available, fall back to HTTP.

    Restart Docker for the changes to take effect.

  Repeat these steps on every Engine host that wants to access your registry.

Use self-signed certificates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  *Warning: Using this along with basic authentication requires to also trust the certificate into the OS cert store for some versions of docker (see below)*

  This is more secure than the insecure registry solution.

    1. Generate your own certificate::

      $ mkdir -p certs

      $ openssl req \
        -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key \
        -x509 -days 365 -out certs/domain.crt

    Be sure to use the name **myregistrydomain.com** as a CN.

    2. Use the result to start your registry with TLS enabled.

    3. Instruct every Docker daemon to trust that certificate. The way to do this depends on your OS.

        + Linux: Copy the domain.crt file to /etc/docker/certs.d/myregistrydomain.com:5000/ca.crt on every Docker host. You do not need to restart Docker.

        + Windows Server:

            1. Open Windows Explorer, right-click the domain.crt file, and choose Install certificate. When prompted, select the following options:

              Store location 	local machine
              Place all certificates in the following store 	selected

            2. Click Browser and select Trusted Root Certificate Authorities.

            3. Click Finish. Restart Docker.

        + *Docker for Mac*: Follow the instructions on Adding custom CA certificates. Restart Docker.

        + *Docker for Windows*: Follow the instructions on Adding custom CA certificates. Restart Docker.

        注意在 Linux 系统下

        1. 在每个docker 终端 /etc/docker/ 目录下创建 certs.d/serverIP(DNS):port/ 目录
        2. 将个人认证证书（如 domain.crt) 拷贝到  /etc/docker/certs.d/172.19.7.109:5001/目录下
        3. 不需要重启 docker

Native basic auth
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


基础的基于用户名和密码的认证由 docker 镜像库本地认证。
The simplest way to achieve access restriction is through basic authentication (this is very similar to other web servers’ basic authentication mechanism). This example uses native basic authentication using htpasswd to store the secrets.

    *Warning: You cannot use authentication with authentication schemes that send credentials as clear text. You must configure TLS first for authentication to work.*

    1. Create a password file with one entry for the user testuser, with password testpassword::

      $ mkdir auth
      $ docker run \
        --entrypoint htpasswd \
        registry:2 -Bbn testuser testpassword > auth/htpasswd

    2. Stop the registry.

      $ docker container stop registry

    3. Start the registry with basic authentication::

      $ docker run -d \
        -p 5000:5000 \
        --restart=always \
        --name registry \
        -v `pwd`/auth:/auth \
        -e "REGISTRY_AUTH=htpasswd" \
        -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
        -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
        -v `pwd`/certs:/certs \
        -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
        -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
        registry:2

    4. Try to pull an image from the registry, or push an image to the registry. These commands fail.

    5. Log in to the registry::

      $ docker login myregistrydomain.com:5000

    Provide the username and password from the first step.

    Test that you can now pull an image from the registry or push an image to the registry.

    **X509 errors: X509 errors usually indicate that you are attempting to use a self-signed certificate without configuring the Docker daemon correctly. See run an insecure registry.**

总结步骤：

   1. 安装 docker，docker-compose

   2. 创建 registry 目录，本地认证目录 auth/ ,CA 目录 certs/

   3. 生成本地账号::

       $ mkdir auth
       $ docker run \
         --entrypoint htpasswd \
         registry:2 -Bbn testuser testpassword > auth/htpasswd

   4. 申请 公共机构CA证书 或个人认证证书::

       $ mkdir -p certs

       $ openssl req \
         -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key \
         -x509 -days 365 -out certs/domain.crt

   5. 指配 端口，存储卷，证书目录，IP 或域名::

       $ docker service create \
         --name registry \
         --secret domain.crt \
         --secret domain.key \
         --constraint 'node.labels.registry==true' \
         --mount type=bind,src=/mnt/registry,dst=/var/lib/registry \
         -e REGISTRY_HTTP_ADDR=0.0.0.0:80 \
         -e REGISTRY_HTTP_TLS_CERTIFICATE=/run/secrets/domain.crt \
         -e REGISTRY_HTTP_TLS_KEY=/run/secrets/domain.key \
         --publish published=80,target=80 \
         --replicas 1 \
         registry:2

       OR:

       $ docker run -d \
         -p 5000:5000 \
         --restart=always \
         --name registry \
         -v `pwd`/auth:/auth \
         -e "REGISTRY_AUTH=htpasswd" \
         -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
         -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
         -v `pwd`/certs:/certs \
         -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
         -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
         registry:2

   6. 启用镜像 docker run 或启用服务 docker service create

    配置 YML::

      registry:
      restart: always
      image: registry:2
      ports:
       - 5000:5000
      environment:
       REGISTRY_HTTP_TLS_CERTIFICATE: /certs/domain.crt
       REGISTRY_HTTP_TLS_KEY: /certs/domain.key
       REGISTRY_AUTH: htpasswd
       REGISTRY_AUTH_HTPASSWD_PATH: /auth/htpasswd
       REGISTRY_AUTH_HTPASSWD_REALM: Registry Realm
      volumes:
       - /path/data:/var/lib/registry
       - /path/certs:/certs
       - /path/auth:/auth


    Replace /path with the directory which contains the certs/ and auth/ directories.

    Start your registry by issuing the following command in the directory containing the docker-compose.yml file::

      $ docker-compose up -d


   参考链接： 部署镜像库：https://docs.docker.com/registry/deploying/#storage-customization
             测试镜像库安装：https://docs.docker.com/registry/insecure/#use-self-signed-certificates
