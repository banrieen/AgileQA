==========================
Android ADB 常用方法
==========================

-----------------
ADB 管理安装包
-----------------

列出只包含 “suneee" 的第三方包
============================
* 以查找 "suneee" 软件包为例::

    adb shell pm list packages suneee -3


-----------------
ADB 管理存储
-----------------


管理SD卡
=============

* 填充SD卡，内存填充可能要3min，不同Android样机的SD卡挂在目录可能不同::

   adb shell dd if=/dev/zero of=/storage/sdcard0/dummy_file bs=4000000000 count=1
   adb shell dd if=/dev/zero of=/mnt/sdcard/bigfile
   填充之后的清楚
   adb shell rm -rf /mnt/sdcard/bigfile
   adb shell rm -rf /storage/sdcard0/dummy_file
