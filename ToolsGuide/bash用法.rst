===================================================
Bash 常用方法
===================================================


让进程在后台可靠运行的几种方法
===================================================
我们经常会碰到这样的问题，用 telnet/ssh 登录了远程的 Linux 服务器，运行了一些耗时较长的任务， 结果却由于网络的不稳定导致任务中途失败。
如何让命令提交后不受本地关闭终端窗口/网络断开连接的干扰呢？下面举了一些例子， 您可以针对不同的场景选择不同的方式来处理这个问题。

nohup/setsid/&
---------------------------------------------------

**场景：**

如果只是临时有一个命令需要长时间运行，什么方法能最简便的保证它在后台稳定运行呢？

**解决方法：**

我们知道，当用户注销（logout）或者网络断开时，终端会收到 HUP（hangup）信号从而关闭其所有子进程。因此，我们的解决办法就有两种途径：要么让进程忽略 HUP 信号，要么让进程运行在新的会话里从而成为不属于此终端的子进程。

**hangup 名称的来由**

在 Unix 的早期版本中，每个终端都会通过 modem 和系统通讯。当用户 logout 时，modem 就会挂断（hang up）电话。 同理，当 modem 断开连接时，就会给终端发送 hangup 信号来通知其关闭所有子进程。

1. nohup

  nohup 无疑是我们首先想到的办法。顾名思义，nohup 的用途就是让提交的命令忽略 hangup 信号。让我们先来看一下 nohup 的帮助信息：
::
    NOHUP(1)                        User Commands                        NOHUP(1)

    NAME
           nohup - run a command immune to hangups, with output to a non-tty

    SYNOPSIS
           nohup COMMAND [ARG]...
           nohup OPTION

    DESCRIPTION
           Run COMMAND, ignoring hangup signals.

           --help display this help and exit

           --version
                  output version information and exit

可见，nohup 的使用是十分方便的，只需在要处理的命令前加上 nohup 即可，标准输出和标准错误缺省会被重定向到 nohup.out 文件中。一般我们可在结尾加上"&"来将命令同时放入后台运行，也可用">filename 2>&1"来更改缺省的重定向文件名。

nohup 示例
::
    [root@pvcent107 ~]# nohup ping www.ibm.com &
    [1] 3059
    nohup: appending output to `nohup.out'
    [root@pvcent107 ~]# ps -ef |grep 3059
    root      3059   984  0 21:06 pts/3    00:00:00 ping www.ibm.com
    root      3067   984  0 21:06 pts/3    00:00:00 grep 3059
    [root@pvcent107 ~]#

2. setsid

nohup 无疑能通过忽略 HUP 信号来使我们的进程避免中途被中断，但如果我们换个角度思考，如果我们的进程不属于接受 HUP 信号的终端的子进程，那么自然也就不会受到 HUP 信号的影响了。setsid 就能帮助我们做到这一点。让我们先来看一下 setsid 的帮助信息：
::
    SETSID(8)                 Linux Programmer’s Manual                 SETSID(8)

    NAME
           setsid - run a program in a new session

    SYNOPSIS
           setsid program [ arg ... ]

    DESCRIPTION
           setsid runs a program in a new session.

可见 setsid 的使用也是非常方便的，也只需在要处理的命令前加上 setsid 即可。
setsid 示例::
    [root@pvcent107 ~]# setsid ping www.ibm.com
    [root@pvcent107 ~]# ps -ef |grep www.ibm.com
    root     31094     1  0 07:28 ?        00:00:00 ping www.ibm.com
    root     31102 29217  0 07:29 pts/4    00:00:00 grep www.ibm.com
    [root@pvcent107 ~]#

值得注意的是，上例中我们的进程 ID(PID)为31094，而它的父 ID（PPID）为1（即为 init 进程 ID），并不是当前终端的进程 ID。请将此例与nohup 例中的父 ID 做比较。

3. &

这里还有一个关于 subshell 的小技巧。我们知道，将一个或多个命名包含在“()”中就能让这些命令在子 shell 中运行中，从而扩展出很多有趣的功能，我们现在要讨论的就是其中之一。
当我们将"&"也放入“()”内之后，我们就会发现所提交的作业并不在作业列表中，也就是说，是无法通过jobs来查看的。让我们来看看为什么这样就能躲过 HUP 信号的影响吧。
subshell 示例::

    [root@pvcent107 ~]# (ping www.ibm.com &)
    [root@pvcent107 ~]# ps -ef |grep www.ibm.com
    root     16270     1  0 14:13 pts/4    00:00:00 ping www.ibm.com
    root     16278 15362  0 14:13 pts/4    00:00:00 grep www.ibm.com
    [root@pvcent107 ~]#

从上例中可以看出，新提交的进程的父 ID（PPID）为1（init 进程的 PID），并不是当前终端的进程 ID。因此并不属于当前终端的子进程，从而也就不会受到当前终端的 HUP 信号的影响了。


disown
---------------------------------------------------------------------

**场景：**

我们已经知道，如果事先在命令前加上 nohup 或者 setsid 就可以避免 HUP 信号的影响。但是如果我们未加任何处理就已经提交了命令，该如何补救才能让它避免 HUP 信号的影响呢？
**解决方法：**

这时想加 nohup 或者 setsid 已经为时已晚，只能通过作业调度和 disown 来解决这个问题了。让我们来看一下 disown 的帮助信息：
::
    disown [-ar] [-h] [jobspec ...]
        Without options, each jobspec is  removed  from  the  table  of
        active  jobs.   If  the -h option is given, each jobspec is not
        removed from the table, but is marked so  that  SIGHUP  is  not
        sent  to the job if the shell receives a SIGHUP.  If no jobspec
        is present, and neither the -a nor the -r option  is  supplied,
        the  current  job  is  used.  If no jobspec is supplied, the -a
        option means to remove or mark all jobs; the -r option  without
        a  jobspec  argument  restricts operation to running jobs.  The
        return value is 0 unless a jobspec does  not  specify  a  valid
        job.

可以看出，我们可以用如下方式来达成我们的目的。
灵活运用 CTRL-z

在我们的日常工作中，我们可以用 CTRL-z 来将当前进程挂起到后台暂停运行，执行一些别的操作，然后再用 fg 来将挂起的进程重新放回前台（也可用 bg 来将挂起的进程放在后台）继续运行。这样我们就可以在一个终端内灵活切换运行多个任务，这一点在调试代码时尤为有用。因为将代码编辑器挂起到后台再重新放回时，光标定位仍然停留在上次挂起时的位置，避免了重新定位的麻烦。

  + 用disown -h jobspec来使某个作业忽略HUP信号。
  + 用disown -ah 来使所有的作业都忽略HUP信号。
  + 用disown -rh 来使正在运行的作业忽略HUP信号。

需要注意的是，当使用过 disown 之后，会将把目标作业从作业列表中移除，我们将不能再使用jobs来查看它，但是依然能够用ps -ef查找到它。

但是还有一个问题，这种方法的操作对象是作业，如果我们在运行命令时在结尾加了"&"来使它成为一个作业并在后台运行，那么就万事大吉了，我们可以通过jobs命令来得到所有作业的列表。但是如果并没有把当前命令作为作业来运行，如何才能得到它的作业号呢？答案就是用 CTRL-z（按住Ctrl键的同时按住z键）了！

CTRL-z 的用途就是将当前进程挂起（Suspend），然后我们就可以用jobs命令来查询它的作业号，再用bg jobspec来将它放入后台并继续运行。需要注意的是，如果挂起会影响当前进程的运行结果，请慎用此方法。
disown 示例1（如果提交命令时已经用“&”将命令放入后台运行，则可以直接使用“disown”）::

    [root@pvcent107 build]# cp -r testLargeFile largeFile &
    [1] 4825
    [root@pvcent107 build]# jobs
    [1]+  Running                 cp -i -r testLargeFile largeFile &
    [root@pvcent107 build]# disown -h %1
    [root@pvcent107 build]# ps -ef |grep largeFile
    root      4825   968  1 09:46 pts/4    00:00:00 cp -i -r testLargeFile largeFile
    root      4853   968  0 09:46 pts/4    00:00:00 grep largeFile
    [root@pvcent107 build]# logout

disown 示例2（如果提交命令时未使用“&”将命令放入后台运行，可使用 CTRL-z 和“bg”将其放入后台，再使用“disown”）::

    [root@pvcent107 build]# cp -r testLargeFile largeFile2

    [1]+  Stopped                 cp -i -r testLargeFile largeFile2
    [root@pvcent107 build]# bg %1
    [1]+ cp -i -r testLargeFile largeFile2 &
    [root@pvcent107 build]# jobs
    [1]+  Running                 cp -i -r testLargeFile largeFile2 &
    [root@pvcent107 build]# disown -h %1
    [root@pvcent107 build]# ps -ef |grep largeFile2
    root      5790  5577  1 10:04 pts/3    00:00:00 cp -i -r testLargeFile largeFile2
    root      5824  5577  0 10:05 pts/3    00:00:00 grep largeFile2
    [root@pvcent107 build]#

--------------------

``参考：`` [1] Linux_ 技巧：让进程在后台可靠运行的几种方法

.. _Linux: https://www.ibm.com/developerworks/cn/linux/l-cn-nohup/index.html
