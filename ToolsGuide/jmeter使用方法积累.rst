-----------------------------------------------------------
jmeter使用方法积累
-----------------------------------------------------------


Function helper
-----------------------------------------------------------

函数使用帮助器，当不确定函数的使用方法和预期结果的使用，可以使用Function Helper 初始化函数，
查看执行后的结果，是否符合预期。

1. 使用方法
  打开工具栏的option 菜单，选择第一个 Function Hepler Dialog即可

  示例：如需要以秒计算的时间值

  在Function helper Dialog 选择 __time 点击Generation 查看用法和预期结果


2. 随机数，随机字符串

  ${__Random(1500,5000,)} 在1500 ~ 5000 内的随机数字

  ${__RandomString(6,a12zeczclk, MYVAR)} 在字符串“a12zeczclk”中随机选择6个字符，存入变量MYVAR 中

Custom JMeter Functions
===========================================================

`Jmeter 扩展函数`_

**chooseRandomsince**

This function choose single random value from the list of its arguments. The last argument is not taken into choice, it is interpreted as variable name to store the result.

Parameters:

    First value to choose from
    Second value to choose from
    ... any number of further choices ...
    Mandatory variable to store result

Example, choosing random color and assigning it to randomColor variable:
::
  ${__chooseRandom(red,green,blue,orange,violet,magenta,randomColor)}

**doubleSum**

This function used to compute the sum of two or more floating point values.

Parameters:

  First value to sum - required

  Second value to sim - required

  More values to sum - optional

  Last argument - variable name to store the result

Example, returning 8.3 and also saving it to variable sumVariable:
::
  ${__doubleSum(3.5, 4.7, sumVariable)}


**MD5**

This function used to calculate MD5 hash of constant string or variable value.

Parameters:

  First value is string constant, variable, or function call - required

  Second argument - variable name to store the result

Example, calculating MD5 for 'test':

  ${__MD5(test)}


.. _Jmeter 扩展函数:: https://jmeter-plugins.org/wiki/Functions/#MD5supfont-color-gray-size-1-since-0-4-2-font-sup


用户密码 MD5 序列化
===========================================================

Jmeter 没有原生支持 MD5 函数，需要添加 jmeter-plugins-functions-2.0.jar 插件 到/libs/ext/下

当然也可以使用javascripts ，Groory 脚本，beanshell 脚本，自定义java函数等。

以 jmeter-plugins-functions 的 MD5 函数为例：

::
    ${__MD5(${passwd})}

用户签名 sign 序列化过程
===========================================================

以象普框架入口为例：（使用到了AES加密伪造较为困难）

入参::

    ${username},${passwd} = CSV Data Set Config (${confpath}tester_account0.csv)

    {"account":"${username}",
    "appCode":"WEININGAPP",
    "clientIp":"127.0.0.1",
    "encryptCode":"1234567899876543",
    "enterpriseCode":"WEINING",
    "entrance":"HAI",
    "password":"${__MD5(${passwd})}

页面内加载的javascripts::

  sign = Encrypt(JSON.stringify(parameter()));
	function parameter() {
		return {
			account:account,
			appCode:appCode,
			clientIp:clientIp,
			encryptCode:encryptCode,
			enterpriseCode:enterpriseCode,
			entrance:'HAI',
			password:password
		};
  function Encrypt(word){
   	var key = CryptoJS.enc.Utf8.parse(encryptCode);
      var srcs = CryptoJS.enc.Utf8.parse(word);

      var encrypted = CryptoJS.AES.encrypt(srcs, key, { mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
  return encrypted.toString();
  }

附 页面加载的Aes.js info::

      /*
    CryptoJS v3.1.2
    code.google.com/p/crypto-js
    (c) 2009-2013 by Jeff Mott. All rights reserved.
    code.google.com/p/crypto-js/wiki/License
    */


逻辑控制器
-----------------------------------------------------------

1. Once Only Controller
  加入该逻辑控制器中的请求仅执行一次。

  当我们在验证某功能与账户登录无关，但操作必须要用户登录鉴权，这时可将用户登录鉴权部分请求放入这个控制器中，
  让其在并发中只执行一次，使用HTTP Cookie Manager 维护登录状态。

2. If Controller
     通过判定condition（条件）是否成立，来执行该控制器内的请求。

3. Loop Controller
    可以指定该控制器内的请求循环执行的次数

jmeter Timer
-----------------------------------------------------------

  在Thread delay 中设置Constant Timer为固定时间就是固定时长，在Thread 管理域内的每次操作之后都会执行该延时

  使用函数或变量 ${__Random(1500,5000,)} 设置为随机时间，则每次延时是随机范围的


How to use Timers in Jmeter
===========================================================
By default, JMeter sends the request **without pausing** between each request. In that case, JMeter could **overwhelm** your test server by making too many requests in a short amount of times.

Let imagine that you send **thousands** request to a web server under test in a few seconds. This is what happens!

.. image:: images/jmeter_TimerOverload.png


`Timers`_ allow JMeter to delay between each request which a thread makes. Timer can solve the server overload problem.

.. _Timers:: https://www.guru99.com/timers-jmeter.html

Also, in real life visitors do not arrive at a website all at the same time, but at different time intervals. So Timer will help mimic the real time behavior.

Constant Timer
===========================================================


First of all, I would like to recommend you to watch the two GIF below:



.. image:: images/jmeter_without_timer.gif

.. image:: images/jmeter_with_timer.gif

GIF 1 represents the default of JMeter, all requests will be sent without pausing between them. This might cause server overload easily by making too many requests in a few seconds.

You know, in the real life, the users don’t visit the website at the same time and they will not do the actions continuously. They need some time to wait for the page load, take a look at the page content before going to the next step/action. I.E. When a user is staying at Register page, he/she might take a few minutes to complete inputting the information and then click the Submit button. Another example, a user go to the post A, reading the content in few second before going to the next post.

So users will do the action at different time intervals, between each request must have a delay time. In the world of load testing, this delay is known as `User’s Think Time`_.

Through the context above, while developing Test Plan in JMeter, we should not let it run the test by default, instead, we need an element which **helps to mimic the real time behavior**. It’s **TIMER**.

Timers can help to simulate a virtual user’s “think time”.Timers allows JMeter to **delay/pause** between each request which a thread makes.

.. _User’s Think Time:: https://jmetervn.com/2017/06/04/constant-timer/

Jmeter 请求响应时间比浏览器的时间短？
===========================================================

Here, when i use my app in real time it take too much time(> 2 sec) to load from one page to another. But, in jmeter the results shows that the pages loads in quick time(avg time - 668 ms).

* There are some reasons why JMeter is faster:

  + Jmeter opens only html page, browser opens page with pictures and with another stuff
  + Jmeter doesn't render html and JS, but browser does

* Make some changes to your JMeter script:

  + Add HTTP Cookie Manager

  + Add HTTP Cache Manager

  + Add HTTP Request Defaults

  + Move Login page as child into Once Only Controller (as you won't login each time, right?)


异常情况
-----------------------------------------------------------

How do I parameterize my JMeter test cases?
===============================================================================

  Answer: Parameters can be set at both the Test Plan and Thread Group levels.

  At the Test Plan level, parameters can be used as constants to minimize changes throughout the test plan when a server or port changes, for example. [N.B. functions are not currently supported at Test Plan level.]

  Within Thread Groups, the *User Parameters* in *Pre-Processor* can be used to set different parameters for each simulated user.


Does JMeter process dynamic pages (e.g. Javascript and capplets)
===============================================================================

  No. JMeter does not process Javascript or applets embedded in HTML pages.

  JMeter can download the relevant resources (some embedded resources are downloaded automatically if the correct options are set), but it does not process the HTML and execute any Javascript functions.

  If the page uses Javascript to build up a URL or submit a form, you can use the Proxy Recording facility to create the necessary sampler. If this is not possible, then manual inspection of the code may be needed to determine what the Javascript is doing.

property function
-----------------------------------------------------------
通过 property 定制参数，将脚本中得csv文件路径，线程数，循环数，时间设置参数化；独立于业务场景。

1. __property 函数，能够用来处理默认参数，示例如下::

  #The property function returns the value of a JMeter property. If the property value cannot be found, and no default has been supplied, it returns the property name. When supplying a default value, there is no need to provide a function name - the parameter can be set to null, and it will be ignored.

    ${__property(user.dir)} - return value of user.dir
    ${__property(user.dir,UDIR)} - return value of user.dir and save in UDIR
    ${__property(abcd,ABCD,atod)} - return value of property abcd (or "atod" if not defined) and save in ABCD
    ${__property(abcd,,atod)} - return value of property abcd (or "atod" if not defined) but don't save it


2. __P 函数能够实现在 non-GUI模式下，通过 command-line 传参，参数化配置脚本::

    #This is a simplified property function which is intended for use with properties defined on the command line. Unlike the __property function, there is no option to save the value in a variable, and if no default value is supplied, it is assumed to be 1. The value of 1 was chosen because it is valid for common test variables such as loops, thread count, ramp up etc.

    Define the property value:

      jmeter -Jgroup1.threads=7 -Jhostname1=www.realhost.edu

    Fetch the values:
    ${__P(group1.threads)} - return the value of group1.threads
    ${__P(group1.loops)} - return the value of group1.loops
    ${__P(hostname,www.dummy.org)} - return value of property hostname or www.dummy.org if not defined
    In the examples above, the first function call would return 7, the second would return 1 and the last would return www.dummy.org (unless those properties were defined elsewhere!)

    示例::

       jmeter -n -Jgroup1.threads=5000 -Jgroup1.period=1 -Jgroup1.loops=10 -Jgroup1.csvPath="/home/jmeterEnv/"  -t Upload_images_WeiNingTong.jmx -l /home/report/weiningtong1.jtl  -e -o /home/report/jmeter/

分布式执行
-------------------------------------------------------------

1. Jmeter client 性能没达到预期
2. 网络带宽受限

* 分布式功能

  1. 保存脚本到client 本地
  2. 一台测试机可以管理 多台 JmeterEngines
  3. Client 能将脚本分发到多台服务器。

* 注意：

  + 测试脚本会在所有的remote server 上执行，server 之间不会做负载均衡，如 10个server 执行一个脚本用户数为600，实际为6000.
  + 多个server分布执行的情况下，Client 会耗用更多的资源，可能出现server 正常，而client 负载超出；可以考虑Stripped modes
  + 如果JmeterEngin 和被测试应用在同一个服务器上，那么jmeter 自然会开销应用服务器的资源，对于应用的性能数据会有影响。可以考虑多个应用服务节点

* Remote 节点配置

  1. 每个节点的jmeter版本保持一致
  2. 每个节点的java 版本要保持一致
  3. have a valid keystore for RMI over SSL, or you have disabled the use of SSL.
  4. 如果脚本中用到了一些 数据文件，则要在每个server 同步放在相同的目录下

* 脚本参数设置  （内置函数传递命令行配置 ${__P(VAR)}	）

    group0_threads	 ${__P(group0.threads)}
    group0_period	 ${__P(group0.period)}
    group0_loop	 ${__P(group0.loops)}

* 启用 server

* 启用client


    示例::

       jmeter -n -Jgroup1.threads=5000 -Jgroup1.period=1 -Jgroup1.loops=10 -Jgroup1.csvPath="/home/jmeterEnv/"  -t Upload_images_WeiNingTong.jmx -l /home/report/weiningtong1.jtl  -e -o /home/report/jmeter/  -R10.1.40.46,10.1.40.47
       jmeter -n -Jgroup1.threads=5000 -Jgroup0.period=1 -Jgroup0.loops=10 -Jgroup0.csvPath="/home/jmeterEnv/" -Jremote_hosts={10.1.40.46,10.1.40.47} -t Upload_images_WeiNingTong.jmx -l /home/report/weiningtong1.jtl  -e -o /home/report/jmeter/  -r


Jmeter 的适用范围
======================================
网页请求，web，http请求
App 的网络请求
PC 本地应用的的网络请求


参考：
    1. Jmeter usermanual
    2. Jmeter FAQ

**备注：原文当rst 格式同步于gitlab，如果在转换后的文档 pdf 活动 docx 中发现异常，或疑问请参照原格式文档；或参考资料。 Qinxing 2018-02-01 **
