SaltStack 基本应用
===========================================================

salt-minion install on centos
-----------------------------------------------------------

* Add salt REPL repository
  Modify the vi /etc/yum.repos.d/CentOS-Base.repo and append the::

[saltstack-repo]
name=SaltStack repo for Red Hat Enterprise Linux $releasever
baseurl=https://repo.saltstack.com/yum/redhat/$releasever/$basearch/latest
enabled=1
gpgcheck=1
gpgkey=https://repo.saltstack.com/yum/redhat/$releasever/$basearch/latest/SALTSTACK-GPG-KEY.pub
     https://repo.saltstack.com/yum/redhat/$releasever/$basearch/latest/base/RPM-GPG-KEY-CentOS-7

* Install saltstack dependance

        yum install salt-master
        yum install salt-minion
        yum install salt-ssh
        yum install salt-syndic
        yum install salt-cloud


* Reference

  https://docs.saltstack.com/en/latest/ref/configuration/minion.html

where the configuration will be in /usr/local/etc/salt/minion.

* Get slat-minion status
sudo salt-run manage.status
down:
    - WinServer2016
    - devops-3.novalocal
    - qinxing-win10
up:
    - WeiNingJXSLDbT52
    - zzlTestEnv

* Minion did not return. [No response] Debug:

  sudo salt-Minion

    [runner@AutoStorage ~]$ sudo salt-minion

  [CRITICAL] 'master_type' set to 'failover' but 'retry_dns' is not 0. Setting 're                      try_dns' to 0 to failover to the next master on DNS errors.
  [ERROR   ] The master key has changed, the salt master could have been subverted                      , verify salt master's public key
  [CRITICAL] The Salt Master server's public key did not authenticate!
  The master may need to be updated if it is a version of Salt lower than 2017.7.2                      , or
  If you are confident that you are connecting to a valid Salt Master, then remove                       the master public key and restart the Salt Minion.
  The master public key can be found at:
  /home/runner/etc/salt/pki/minion/minion_master.pub
  [ERROR   ] No master could be reached or all masters denied the minions connecti                      on attempt.
  [ERROR   ] Error while bringing up minion for multi-master. Is master at ['172.1                      9.7.110'] responding?
  [ERROR   ] The master key has changed, the salt master could have been subverted, verify salt master's public key
  [CRITICAL] The Salt Master server's public key did not authenticate!
  The master may need to be updated if it is a version of Salt lower than 2017.7.2, or
  If you are confident that you are connecting to a valid Salt Master, then remove the master public key and restart the Salt Minion.
  The master public key can be found at:
  /home/runner/etc/salt/pki/minion/minion_master.pub
  [ERROR   ] No master could be reached or all masters denied the minions connection attempt.
  [ERROR   ] Error while bringing up minion for multi-master. Is master at ['172.19.7.110'] responding?
  [ERROR   ] The master key has changed, the salt master could have been subverted, verify salt master's public key
  [CRITICAL] The Salt Master server's public key did not authenticate!
  The master may need to be updated if it is a version of Salt lower than 2017.7.2, or
  If you are confident that you are connecting to a valid Salt Master, then remove the master public key and restart the Salt Minion.
  The master public key can be found at:
  /home/runner/etc/salt/pki/minion/minion_master.pub
  [ERROR   ] No master could be reached or all masters denied the minions connection attempt.
  [ERROR   ] Error while bringing up minion for multi-master. Is master at ['172.19.7.110'] responding?
  ^C[WARNING ] Minion received a SIGINT. Exiting.
  The Salt Minion is shutdown. Minion received a SIGINT. Exited.
  [runner@AutoStorage ~]$ sudo rm -rf /home/runner/etc/salt/pki/minion/minion_master.pub
  [runner@AutoStorage ~]$ sudo systemctl restart salt-minion
