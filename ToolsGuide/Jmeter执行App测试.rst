====================================
Jmeter 测试App
====================================

BlazeMeter Wiki
====================================

*Check out this handy dandy video tutorial which shows a step by step how-to setup and run a mobile performance test.
BlazeMeter provides a series of handy options to simulate the work of users with various mobile devices.
As always, we start by creating a test script - we are using Apache JMeter.  But, before the script development we need to configure our mobile device, which will be used for test recording. JMeter records all actions using its own proxy. That is why we need to configure our internet connection in a specific way.
Configure Mobile Device
Open JMeter and add the HTTP Proxy Server component to WorkBench. Then in the component settings, set a port to listen (for example 8089). In the configuration settings of the mobile device, configure the proxy to use as seen in the image below. As a proxy hostname, you will need to set the IP address of the computer on whichever JMeter application is open.
So, follow these steps:

1. Run JMeter application on your computer.
2. Add to JMeter’s test plan HTTP Proxy Server component (right-click on WorkBench – Add – Non-test Elements – HTTP Proxy Server), as shown below:

.. image:: images/JmeterApp01.png

3. In the HTTP Proxy Server component’s global settings, set the port to listen (for example 8089); and click on the Start button to start configured proxy.
4. Get your computer’s IP address. For more details how to do it you can refer directly to the help documentation of whichever operating system that you’re using (for example, for Windows 7 go to http://windows.microsoft.com/is-is/windows-vista/find-your-computers-ip-address ).
5. Under network configuration of your mobile device, set the computer’s IP address as the proxy IP and port that you had set in JMeter.
For Android devices:
a. Go to Options – Wi-Fi, long tap on the current network connection and choose the “Modify Network” popup menu item;
b. Check “Show advanced options” checkbox;
c. Set parameter “Proxy settings” to “Manual”;
d. In the field “Proxy hostname” type the IP of the computer (for example, 10.2.0.89), into the field “Proxy port” our port – 8089;
e. Save changes.

Note that Android supports only HTTP proxy. If your application uses an HTTPS connection, then you may use an additional application that performs with HTTPS proxing.
Run Mobile Web Application Now you can open the web application you want to test and use it as you usually do. For example, we chose an application that streams news. While using the application, you must perform the same actions that are performed by actual users (login, scroll, comment, click varying links, search information, etc.)
As a result, we got full emulation of our application’s connection with its web server. Because we set the mobile connection over JMeter’s proxy, JMeter recorded all network communications of the mobile web application with its server. All actions are recorded to our JMeter test plan. Upon completion of work with the mobile application we can stop the proxy server and configure the mobile network configuration back to normal mode. To stop the JMeter proxy, open HTTP Proxy Server’s options again and press the "Stop" button.
.. image:: images/JmeterApp02.png
After recording actions from the mobile device and final correction of the load script, we can run load test using BlazeMeter's capabilities.
.. image:: images/JmeterApp03.png


How to Test Native App Performance using Apache JMeter?
================================================================================

In my previous blog, I have elucidated how to test web application’s performance using JMeter. Here, I would like to share how to test mobile app’s performance in JMeter.
JMeter records mobile application requests on HTTP(S) Script Recorder and it has features to prepare and run our mobile application performance test scripts. All we have to do is to configure JMeter and Mobile device. In JMeter, we can test both iOS and Android native applications.

Steps to Configure JMeter:
-----------------------------

    Go to apache-jmeter-3.1/bin folder and start the jmeter.bat (Batch) file.
    Right click on “Test Plan” > Add > Threads User(s) > Thread Group.
    Right click on “Thread Group” > Add > Logic Controller > Recording Controller.
    Right click on “Thread Group” > Add > Listeners > View Results in Tree.
    Add HTTP(s) Script Recorder to Workbench by selecting Workbench > Add > Non-Test Elements > HTTP(s) Script Recorder option.
    Add Port value as 8080 and click “START” button in script recorder. The JMeter proxy will be started on localhost.

Note: If you are testing from behind a firewall/proxy server, you may need to run the jmeter.bat file from a command line mode to use following parameters (If you are working under proxy network in office, use this method.)

Go to jmeter/bin directory and enter this:

jmeter.bat -H <Hostname or ip address> -P <Server port> -N <Non proxy hosts> -u <Username for proxy – If required> -a <Password for proxy – if required>

Example: jmeter.bat -H my.proxy.server -P 9090  -u username -a password -N localhost

Next, we need to configure the mobile device as per the following steps.

Before starting the configuration on your device, you should install “ApacheJmeterTemporaryRootCA.crt” certificate in your mobile/tab device.

Steps to install the certificate:

1. Go to jmeter/bin directory and find the ApacheJmeterTemporaryRootCA.crt file. (If you cannot find the certificate from bin directory, just go to your HTTP(S) script recorder screen in Jmeter and click ‘Start’ button. It will generate new certificate on your bin directory automatically).

2. Attach that file and send an email to yourself.

3. Download from your mail and then install on your device.

After installing successfully, you will see the notification “Network may be mounted” on android device. Here you can verify whether the certificate has been installed. For iOS device, it will show you the “Profile Installed” pop-up and you can see the ‘Verified’ sign. Tap ‘Done’ at the top right corner to complete the installation.

Note: Ensure to check whether the certificate has been expired or not. Certificate will be expired within 7 days from the date of generated. If it has expired, simply re-generate again and use it.

iOS Device Proxy Configuration:
------------------------------------------

    Go to Settings > Wi-Fi option. (Note: Network should be same for both Mobile and Laptop/Desktop).
    Click on the Connected network.
    Select “Manual” option from HTTP Proxy.
    Set ‘Server value as system IP address and Port value to 8080 as per JMeter setup.
    Now start the application on the device and its each request will be recorded in JMeter.

.. image:: images/jmeter-app-ios01.png

Android Device Proxy Configuration:
-------------------------------------------

#1. Go to Settings > Wi-Fi option.

#2. Press and hold on connected network and Click ‘Modify Network’ option.

#3. This will open advanced settings from where we can modify Proxy.

#4. Change the Proxy to Manual.
.. image:: images/jmeter-app-ios02.png

#5. Set Hostname value as system IP address and Port value to 8080 as per JMeter setup.

.. image:: images/jmeter-app-ios03.png

#6. Click on ‘Save’.

#7. Now start the application on the device and its each request will be recorded in JMeter.

#8. Before running the application, make sure that HTTP(S) script recorder was started in JMeter.

#9. Once the application is started, the actions of the script will be recorded under “Recording Controller.”

#10. Once actions are done in the application, stop the script recording by clicking ‘Stop’ button in HTTP(S) script recorder screen.

#11. Now, expand the “Recording Controller” in JMeter. You can view all the Recorded scripts of whatever actions are done in the applications.

#12. Finally, click the ‘Play’ button. It will run recorded scripts as per your Threads setup.

#13. Click on “View Results in Tree”. Here you will see the result of your script.


Solutions for issues while testing the mobile application:
--------------------------------------------------------------------------------

Here are the solutions for the issues that we have noticed while doing the load test in JMeter.

**Issue 1**

The application wasn’t getting loaded.

**Solution**

The issue is caused because our system was enabled with the firewall protection in security software. This is restricting network connection when setup with the proxy. We have disabled the firewall protection in antivirus settings.

**Issue 2**

Internet was not working with Proxy on the mobile device when we configure the mobile device and JMeter.

**Solution**

The reason might be that the system IP address is not configured with JMeter properties which mean the current IP address is not added to the JMeter properties file. We have added remote host IP address to property file as per the following steps.

  1. Open the browser and type IP address and Port number that is mentioned in JMeter to check whether it was configured. If browser shows Connect, then it is working fine otherwise if it is continuously loading, then it was not configured. Then we have to follow the below steps:
  2. Open apache-jmeter folder from the local drive and go to the bin folder.
  3. Open “jmeter (.properties)” file and find “Remote hosts and RMI configuration” section on this file.
  4. Under this section, add the system IP to remote hosts with comma delimited and save the file.
