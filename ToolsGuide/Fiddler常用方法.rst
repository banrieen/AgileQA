
==============================
Fiddler 使用方法
==============================

------------------------------
fiddler 添加查看 IP 和响应时间
------------------------------

fiddler 查看 IP 地址
======================

1. 点击菜单栏 rules——customize rules...
2. ctrl+f 搜索 “static function main”
3. 在main函数里加入下面一行代码，调用fiddlerUI函数，显示ip地址列::

    FiddlerObject.UI.lvSessions.AddBoundColumn("ServerIP",120,"X-HostIP");


4. 保存并关闭，重启 fiddler 后即可看到ip地址列

Fiddler 添加查看响应时间
============================

1. 在 Tool bar 上面找到 Rules->CustomRules
2. 在 class Handlers{里面添加}::

    function BeginRequestTime(oS: Session)
    {
        if (oS.Timers != null)
        {
            return oS.Timers.ClientBeginRequest.ToString();
        }
        return String.Empty;
    }


     public static BindUIColumn("Time Taken")
               function CalcTimingCol(oS: Session){
                 var sResult = String.Empty;
                 if ((oS.Timers.ServerDoneResponse > oS.Timers.ClientDoneRequest))
                 {
                   sResult = (oS.Timers.ServerDoneResponse - oS.Timers.ClientDoneRequest).ToString();
                 }
                 return sResult;
               }


-----------------------------------------------
使用 CCproxy 代理局域网，抓取移动端http 请求
-----------------------------------------------

CCproxy 代理局域网设置
============================

1. 在 Windows 桌面系统下载安装 CCproxy
2. 使用 powershell 查找系统所在局域网 IP::

    PS C:\> netsh interface ip show add
    接口 "WAN" 的配置
        DHCP 已启用:                          是
        IP 地址:                           172.19.5.179
        子网前缀:                        172.19.5.0/24 (掩码 255.255.255.0)
        默认网关:                         172.19.5.1
        网关跃点数:                       0
        InterfaceMetric:                      25


    接口 "Loopback Pseudo-Interface 1" 的配置
        DHCP 已启用:                          否
        IP 地址:                           127.0.0.1
        子网前缀:                        127.0.0.0/8 (掩码 255.0.0.0)
        InterfaceMetric:                      75

3. 以管理员方式打开 CCproxy
   * 设置其代理局域网IP为PC所在的局域网，如 172.19.5.179 为办公网络，端口好设置为 808。

   *注意，不要选择“启动”，否则Fiddler 抓取不到报文。*

     .. image:: images/CCproxy_SetLanPort.png

4. 如果 PC 装了 Virtual Box，或 Genymotion 请禁用 VirtualBox 的虚拟网络接口，
   *保持 CCproxy 代理的网络与测试网路一致*::

     ##请使用管理员启用 Powershell 方式
     PS C:\> netsh interface set interface "VirtualBox Host-Only Network" disable
     PS C:\> netsh interface set interface "VirtualBox Host-Only Network #2" disable


Fiddler 配置
====================================================================

在菜单栏tools -> options -> connections 中设置相同的端口为 808
*也可以在 CCproxy , Fiddler 统一设置为8888*

**其他配置参照以下截图即可**

  .. image:: images/Fiddler_SetLanConnection.png
