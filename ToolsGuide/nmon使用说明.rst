==================
nmon 使用说明
==================

1. 根据系统版本环境安装
-------------------------

   如果是最新版本，可以下载最新的nmon安装，如果是其他版本如centos 6.5/6 则需要下载支持该版本的nmon16e_x86_rhel65
   最新版Centos7 下载 nmon16g_x86_rhel72 ，赋予执行权限即可

2. 输出 csv 文件
--------------------------
  ::

    ./nmon -s 5 -t  -f
     #会在当前目录下生成csv格式文件： devops-2_171024_0346.nmon


3. 合并namon csv 结果之 nmonmerge
---------------------------------------
  ::

    #Example: to merge three files a.nmon, b.nmon and c.nmon
      nmonmerge -a a.nmon b.nmon
      nmonmerge -a a.nmon c.nmon
    #Now a.nmon contains all the data

    #Example: to merge three files a.nmon, b.nmon and c.nmon
      nmonmerge a.nmon b.nmon >merged.nmon
      nmonmerge -a merged.nmon c.nmon
    #Now merged.nmon contains all the data


4. 生成图表
------------------

* 之 nmonchart::

    #nmonchart <nmon-file> <output-file>.html
    ./nmonchart sampleC.nmon sampleC.html

* 之 TOPASchart::

    topaschart <nmon-file> <output-file>
    topaschart blue_150508_0800.topas,csv blue_150508_0800.html
    #执行时如果出现：-bash: ./topaschart: /usr/bin/ksh: bad interpreter: No such file or directory
    #请直接安装 ksh 包：yum install ksh -y 解决问题。


----------------------------
nmon linux 下具体使用方法
----------------------------


在线显示模式
-----------------

1. 直接启用nmon ，按 'q' 退出，按h显示帮助信息，再次按'h'退出帮助
2. 按照帮助信息，按'C'显示cpu状态,'n'显示网络状态,'m' 显示内存状态等，再次按'c','n','m'退出显示，屏幕以按键的先后顺序显示相应的信息
3. 屏幕比较大，或x-windows 设置区域比较大，显示信息相对会更多
4. 对于top 进程显示会有不同

抓取数据到csv 文件的方式
-----------------------------

1.  简单的示例： nmon -f -s2 -c 30
2. -f 选项为希望将抓取的数据保存到csv文件中，而不是显示到屏幕上
3. -s 2 表示每个2秒抓取一次
4. -c 30 表示你希望有30个数据点 或映像快照
   -s 2 -c 30 表示nmon 将每隔2s 执行30次结束
5. nmon 在启动时检查系统之后便会断开终端会话，看起来好像在后台执行一样
6. 当nmon 执行完成后，可以使用'ls -l | grep *.nmon' 查找抓取到的数据文件为csv 格式，状态名称与参数值用','分割，可以使用excel，vi,atom等查看

* 可以使用 'ps aux | grep nmon' 确认nmon 是否在执行
* 注意：在nmon 没有执行完成前，再次执行nmon 会有冲突，抓取的数据文件为空，数据分析工具会出错
