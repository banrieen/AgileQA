<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1519717831017" ID="ID_1013198821" MODIFIED="1519718599184" TEXT="SWORT-AutoPlatform">
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1519717851331" ID="ID_1644430540" MODIFIED="1519718401790" POSITION="right" TEXT="&#x5f31;&#x52bf;">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1519718118539" ID="ID_1637790262" MODIFIED="1519718401791" TEXT="&#x6df1;&#x5ea6;&#x4e0d;&#x591f;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1519718128787" ID="ID_329906268" MODIFIED="1519718401816" TEXT="&#x7f3a;&#x5c11;&#x6210;&#x719f;&#x7684;&#x73af;&#x5883;&#x953b;&#x70bc;&#x7cfb;&#x7edf;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1519718244660" ID="ID_182317453" MODIFIED="1519718401817" TEXT="&#x96c6;&#x6210;&#x5ea6;&#x4f4e;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1519718268828" ID="ID_498197707" MODIFIED="1519718401819" TEXT="&#x7f3a;&#x5c11;&#x4eba;&#x529b;&#xff0c;&#x5b8c;&#x5584;&#x548c;&#x4f18;&#x5316;&#x7cfb;&#x7edf;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1519718033427" ID="ID_1499638209" MODIFIED="1519718401832" POSITION="right" TEXT="&#x5a01;&#x80c1;">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1519718339428" ID="ID_324157943" MODIFIED="1519718401834" TEXT="&#x9700;&#x8981;&#x8f83;&#x597d;&#x7684;&#x80cc;&#x666f;&#x652f;&#x6301;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1519718353933" ID="ID_274186392" MODIFIED="1519718401835" TEXT="&#x9700;&#x8981;&#x7cfb;&#x7edf;&#x5316;&#x7684;&#x6570;&#x636e;&#x6d41;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1519718377044" ID="ID_574590165" MODIFIED="1519718401835" TEXT="&#x89c4;&#x6a21;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1519718017668" ID="ID_1589066908" MODIFIED="1519718401837" POSITION="left" TEXT="&#x4f18;&#x52bf;">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1519718099324" ID="ID_33309394" MODIFIED="1519718401837" TEXT="&#x5e7f;&#x6cdb;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1519718104148" ID="ID_431409471" MODIFIED="1519718401839" TEXT="&#x72ec;&#x7acb;&#x652f;&#x6491;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1519718197388" ID="ID_288477764" MODIFIED="1519718401840" TEXT="&#x4e00;&#x4f53;&#x5316;&#x96c6;&#x6210;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1519718237412" ID="ID_78736006" MODIFIED="1519718401841" TEXT="&#x5206;&#x5e03;&#x5f0f;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1519718051564" ID="ID_1109901197" MODIFIED="1519718401843" POSITION="left" TEXT="&#x673a;&#x4f1a;">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1519718298771" ID="ID_955411016" MODIFIED="1519718401844" TEXT="&#x81ea;&#x52a8;&#x5316;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1519718303868" ID="ID_1598483599" MODIFIED="1519718401845" TEXT="&#x5927;&#x6570;&#x636e;&#x5904;&#x7406;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1519718307092" ID="ID_1285533357" MODIFIED="1519718401845" TEXT="&#x7b97;&#x6cd5;&#x7b97;&#x6cd5;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1519718323587" ID="ID_681441930" MODIFIED="1519718401846" TEXT="&#x4e91;&#x670d;&#x52a1;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1519718331301" ID="ID_1456449494" MODIFIED="1519718401847" TEXT="&#x672c;&#x5730;&#x5316;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
</node>
</map>
