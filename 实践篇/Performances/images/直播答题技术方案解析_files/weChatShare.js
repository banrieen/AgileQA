var WeChatShare = {
    init : function(contentTitle, contentDescription) {
        // check if the page is in wechat environment
        if (window.navigator.userAgent.match(/MicroMessenger/i)) {
            // get wechat share permission
            var requrl = 'https://misc.geekbang.org/wxmisc/wxshare/sign?url=' + encodeURIComponent(window.location.href) + '&mpid=geekbang'
            var req = new XMLHttpRequest()
            req.open("GET", requrl)
            req.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    try {
                        var data = JSON.parse(req.responseText)
                        // check if respose is ok
                        if (data.code !== 0) return
                        // check if wx api exists
                        if (!window.wx) return
                        window.wx.config({
                            debug: false,
                            appId: data.data.appId,
                            timestamp: data.data.timestamp,
                            nonceStr: data.data.nonceStr,
                            signature: data.data.signature,
                            jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
                        })
                    } catch (e) {
                    }
                }
            }
            req.send();

            // config wechat share content
            if (window.wx) {
                window.wx.ready(function () {
                    // share to AppMessage
                    window.wx.onMenuShareTimeline({
                        title: contentTitle,
                        imgUrl: 'https://s3.amazonaws.com/c4media-iem/newsletter/en/infoq_wechat_share_thumbnail.png', // custom img
                        link: window.location.href, // custom link
                        success: function () {},
                        cancel: function () {}
                    })
                    // share to Timeline
                    window.wx.onMenuShareAppMessage({
                            title: contentTitle,
                            desc: contentDescription, // custom description
                            imgUrl: 'https://s3.amazonaws.com/c4media-iem/newsletter/en/infoq_wechat_share_thumbnail.png', // custom img
                            link: window.location.href, // custom link
                            type: 'link',
                            dataUrl: '',
                            success: () => {},
                        cancel: () => {}
                })
                });
            }
        }
    }
}
