===========================================
自动化测试平台用户使用手册
===========================================
全面记录和解释自动化测试平台的信息和配置，使用操作


网络部署
===========================================

测试用的虚拟服务器配置
-------------------------------------------

VM Info
Host     | UserName | UserPassword
---------|----------|---------
172.19.7.110 |root | 11111111
            |runner|runner
172.19.7.103|root|11111111
            |centos|centos

Saltstack 部署
-------------------------------------------

1.  salt-minion lissalt

Host     | IP/DNS | ID
---------|----------|---------
salt-master|172.19.7.110 |master
salt-minion|172.19.5.179|qinxing

## Install Docker CE

1. Add Mariadb Docker

2. Add Redis Docker

3. Add Testlink

4. Add Redis

5. Add gitlab-ce

6. Jenkins slave 一致放在终端用户根目录下的jenkins-slave
7. Jenkins slave 在桌面系统，手工执行时简单启用slave-agent.jnlp 客户端保持连接，正在server（VM) 端,或自动化执行时需要后台执行
8. Jmeter 在VM（Centos）下后台执行方式
    java -jar jenkins-slave/slave.jar -jnlpUrl http://172.19.7.110:8082/computer/VM_Master/slave-agent.jnlp -secret a5d0b8c4bc5933c8915fabea1cbb40f78c70d7e10e4179ab18e31a2072553fd6 -workDir "/home/runner/xtcAuto" -failIfWorkDirIsMissing &



测试工程和工具使用
===========================================

|Testsuites | Counts | Status|
|:-----------|:----------:|:---------|
|0.5版本定子链 | 72 |逻辑复杂，第一阶段先不考虑|
|出差审批流程 | 163 | |
|公办应用|73||
|定子链（组织管理）|326|
|官网（绿色丝路）|46|
|响应中心|139| 多媒体用例需要手工测试协助|
|转子链（制度管理）|228 |出差流程复杂，第一阶段先不考虑|
|指挥中心1.5核心功能回归| 22 | 第一阶段先不考虑|

自动化测试平台（工程）构建
--------------------------------------------

+ Postman http 接口测试
+ Jmeter + Html report + Robot Framework 性能测试
+ Robot Framwork + Python，C#，Java 基础测试框架

# Postman 使用文档 翻译
1. 配置代理抓取 http 请求（PC，移动端wifi）
2. 录制脚本，配置脚本
3. Collection 管理测试脚本，
4. 自动化执行

# Jmeter 使用文档翻译
1. Web Basement
2. Web Advanced
3. DB Request
4. SOAP
5. Html Report
6.

# 基础框架
--------------------------------------------

1. C#， Java， Python，C
2. Rebot Framework
3. libs
