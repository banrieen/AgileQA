# 配置代理抓取Http 请求

1. PC Windows 10

    1.1. 打开 设置 ——> 搜索 代理 ——> 打开手动更改代理设置

    1.2. 找到 手动设置代理 ——> 启用 使用代理服务器 

    1.3. 地址框 中添加 127.0.0.1,端口框 添加 5555 然后退出，即可在Postman 中抓取到本机多有的Http 请求。

2. 移动端设置 iOs,Android
   
    1.1. 移动端和PC端 （Postman 安装） 在同一个wifi LAN中

    1.2. 在移动端 网络设置 ——> wifi已连接 配置窗口中 设置代理服务器为 PC端的IP，端口号为 5555
    
    1.3. 即可在PC 端安装的Postman 抓取到移动端的http请求。

